﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class setactive : MonoBehaviour {

    public GameObject Obj;
    public GameObject topPapir;
    public GameObject botPapir;
    public GameObject head;
    public GameObject player;
    public Object nextScene;

    public Collider topPapirCol;
    public Collider botPapirCol;
    public Collider headCol;
    Collider m_Collider;
    private MeshRenderer m; 

    // Use this for initialization
    void Start () {

        topPapir = Obj.transform.Find("top_rygg/top_papir").gameObject;
        botPapir = Obj.transform.Find("bot_rygg/bot_papir").gameObject;
        head     = GameObject.Find("HeadCol");
        player   = GameObject.Find("Camera (head)");


        topPapirCol = topPapir.GetComponent<Collider>();
        botPapirCol = botPapir.GetComponent<Collider>();
        headCol     = head.GetComponent<Collider>();

        m           = this.GetComponent<MeshRenderer>();
        m_Collider = GetComponent<Collider>();

        //m.material.shader = Shader.Find("_Color");

      //  m.material.SetColor("_Color", Color.yellow);

        //  Debug.Log("Hei");
        // Debug.Log(Vectorr3.Distance(topPapirCol.transform.position, botPapirCol.transform.position).ToString());

    }

    // Update is called once per frame
    void Update () {
        //Debug.Log(Vector3.Distance(topPapirCol.transform.position, botPapirCol.transform.position).ToString());
        if (Vector3.Distance(topPapirCol.transform.position, botPapirCol.transform.position)>0.4)
        {
            m.enabled = true;
            m_Collider.enabled = true; 
            
        }
        else
        {
            m.enabled = false;
            m_Collider.enabled = false; 
        }
    }

    private void OnTriggerEnter(Collider other)
        
    {
        if (other.gameObject.tag == "head") {
            Debug.Log(other.gameObject.name);
            SceneManager.LoadScene(nextScene.name);
        }
    }

}
