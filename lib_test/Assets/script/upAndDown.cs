﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class upAndDown : MonoBehaviour {

    public float speed = 5f;
    public float height = 0.5f;
    Vector3 pos;
    // Use this for initialization
    void Start () {
        pos = this.transform.position;

    }

    // Update is called once per frame
    void Update () {

        //calculate what the new Y position will be
        float newY = Mathf.Sin(Time.time * speed);
        //set the object's Y to the new calculated Y
        transform.position = new Vector3(pos.x , pos.y+newY * height, pos.z) ;

    }
}
