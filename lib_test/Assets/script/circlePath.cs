﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class circlePath : MonoBehaviour {
    public Vector3 pos;
    public float speed;
    public float radius;
    public Vector3 offset;
    public float minmax = 1;

    private float angle;

    // Use this for initialization
    void Start () {
        pos = this.transform.position; 
	}
	
	// Update is called once per frame
	void Update () {

        angle += speed * Time.deltaTime;
        offset = new Vector3(Mathf.Sin(angle), 0, Mathf.Cos(angle)) * (radius+Random.Range(-minmax, minmax));
        transform.position = pos + offset;

    }
}
